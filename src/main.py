from fastapi import FastAPI
from routers import modelo_minuta, objeto_recurso

app = FastAPI()

app.include_router(modelo_minuta.router)
app.include_router(objeto_recurso.router)

@app.get("/")
def root():
    return {"message": "APP is running"}

