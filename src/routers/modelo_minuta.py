from doctest import Example
from fastapi import APIRouter
from pydantic import BaseModel


class ObjetoBase(BaseModel):
    nome: str
    situacao: bool
    numero: int 
    secao_id: int

    class conig:
        schema_extra = {
            "example": { 
                "nome": "Objeto 01",
                "situacacao": True,
                "numero": 25,
                "secao_id": 1,
            }
        }

router = APIRouter()

objeto_database = []

@router.get("/objeto-recurso/", tags=["Modelo Minuta"])
def get_objeto_recurso():
    return {"message": "Busca realizada com sucesso", "status": "sucess", "data": objeto_database}


@router.post("/objeto-recurso/", tags=["Objeto_recurso"])
def post_modelo_minuta(objeto_data: ObjetoBase):
    objeto_database.append(objeto_data)
    return {"status": "sucess", "data": objeto_data, "message": "Modelo cadastrado com sucesso"}
